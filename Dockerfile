FROM maven:3.6.3 AS maven

WORKDIR /usr/src/app
COPY . /usr/src/app
# Compile and package the application to an executable JAR
RUN mvn package

FROM adoptopenjdk/openjdk11:alpine-jre

ARG JAR_FILE=BaziKosarkarskaFed-0.0.1-SNAPSHOT.jar

WORKDIR /opt/app

COPY --from=maven /usr/src/app/target/${JAR_FILE} /opt/app/spring-boot-api-tutorial.jar

ENTRYPOINT ["java","-jar","spring-boot-api-tutorial.jar"]
